module Main where

import Control.Monad.Except (
    ExceptT (ExceptT),
    MonadError (throwError),
    MonadIO (liftIO),
    runExceptT,
    withExceptT,
 )
import qualified Data.ByteString as B
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import Data.Void (Void)
import qualified Data.Yaml as Yaml
import System.Environment (getArgs)
import Tahoe.Announcement (Announcements (..))
import qualified Tahoe.Directory as Directory
import qualified Tahoe.MagicFoldr as MF
import qualified Tahoe.SDMF as SDMF
import Text.Megaparsec (ParseErrorBundle, parse)

data ExampleError
    = ProgramUsageError
    | CollectiveCapParseError (ParseErrorBundle T.Text Void)
    | ServerAnnouncementsParseError Yaml.ParseException
    | GettingFilesError T.Text
    deriving (Show)

data Config = Config
    { serversYamlPath :: FilePath
    , collectiveCap :: String
    }

main :: IO ()
main =
    do
        result <- runExceptT $ do
            Config{serversYamlPath, collectiveCap} <- getConfig
            cap <- withExceptT CollectiveCapParseError $ getCollectiveCap collectiveCap
            (Announcements grid) <- withExceptT ServerAnnouncementsParseError $ loadGrid serversYamlPath
            withExceptT GettingFilesError $ ExceptT $ MF.getFolderFiles grid (MF.MagicFolder cap Nothing)
        case result of
            Right mf -> print . Map.keys $ MF.directoryChildren (mf :: MF.FolderEntry)
            Left ProgramUsageError ->
                putStrLn "Usage: magic-foldr-example <path to servers.yaml> <collective read cap>"
            Left (CollectiveCapParseError e) -> do
                putStrLn "Error parsing collective cap:"
                print e
            Left (ServerAnnouncementsParseError e) -> do
                putStrLn "Error parsing server announcements:"
                print e
            Left (GettingFilesError e) -> do
                putStrLn "Error getting folder files:"
                print e

-- | Read the application configuration from argv.
getConfig :: MonadIO m => ExceptT ExampleError m Config
getConfig = do
    args <- liftIO getArgs
    case args of
        [a, b] -> pure $ Config a b
        _ -> throwError ProgramUsageError

{- | Interpret a string as a Magic-Folder collective directory read
 capability.
-}
getCollectiveCap ::
    MonadIO m =>
    String ->
    ExceptT (ParseErrorBundle T.Text Void) m (Directory.DirectoryCapability SDMF.Reader)
getCollectiveCap = ExceptT . pure . parse Directory.pReadSDMF "collective-cap" . T.pack

-- | Load the announcements for the grid to use for downloads.
loadGrid ::
    MonadIO m =>
    -- | The path to a Tahoe-LAFS-style "servers.yaml" file.
    FilePath ->
    ExceptT Yaml.ParseException m Announcements
loadGrid path = do
    bytes <- liftIO $ B.readFile path
    ExceptT $ pure $ Yaml.decodeEither' bytes
