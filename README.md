# magic-foldr

## What is it?

magic-foldr is a Haskell implementation of Magic-Folder.

### What is the current state?

* There is a high-level API which will read useful information about a single participant's state.
* Merging information from multiple participant remains to be done.
* All write operations remain to be done.

## How do I use it?

See ``example-app/Main.hs`` for a simple example.

## Why does it exist?

A Haskell implementation can be used in places the original Python implementation cannot be
(for example, runtime environments where it is difficult to have a Python interpreter).
Additionally,
with the benefit of the experience gained from creating and maintaining the Python implementation,
a number of implementation decisions can be made differently to produce a more efficient, more flexible, simpler implementation and API.
Also,
the Python implementation claims no public library API for users outside of the Tahoe-LAFS project itself.
