{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Tahoe.MagicFoldr where

import Control.Applicative (some, (<|>))
import Control.Concurrent.Async (mapConcurrently)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Bifunctor (Bifunctor (bimap))
import Data.Either (rights)
import Data.List (find)
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust, mapMaybe)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Void (Void)
import Tahoe.Announcement (StorageServerAnnouncement)
import qualified Tahoe.CHK.Capability as CHK
import Tahoe.CHK.Server (StorageServerID)
import Tahoe.Capability (ConfidentialShowable (confidentiallyShow))
import qualified Tahoe.Directory as Directory
import Tahoe.Download (announcementToImmutableStorageServer, announcementToMutableStorageServer, downloadDirectory)
import qualified Tahoe.SDMF as SDMF
import Text.Megaparsec (ParseErrorBundle, Parsec, anySingle, errorBundlePretty, parse)
import Text.Megaparsec.Char (char)

{- | Represent the path of a file within a Magic-Folder with a separate Text
 value for each of the path's segments.
-}
newtype DecodedPath = DecodedPath [T.Text] deriving (Eq, Ord, Show)

{- | Left of a parse error description and a path entry we tried to parse or
 Right of a parsed path, fully representing an entry's position in a
 directory hierarchy.
-}
type Path = Either (T.Text, T.Text) DecodedPath

{- | Serialize a path to a single Text string suitable for representing the
 path within a Tahoe-LAFS directory representing a single participant's
 folder.
-}
encodePath :: DecodedPath -> T.Text
encodePath (DecodedPath segs) = T.replace "/" "@_" . T.replace "@" "@@" . T.intercalate "/" $ segs

-- | Reverse the encoding done by ``encodePath``.
decodePath :: T.Text -> Path
decodePath mangled = convertErrors . parse pMagicPath "magic path" $ mangled
  where
    pMagicPath :: Parsec Void T.Text [T.Text]
    pMagicPath = T.split (== '/') . T.pack <$> some pPathAtom
    pPathAtom = (pQuote >> (pQuote <|> pSep)) <|> anySingle
    pQuote = char '@'
    pSep = char '_' >> pure '/'

    convertErrors (Left err) = Left (T.pack . errorBundlePretty $ err, mangled)
    convertErrors (Right path) = Right $ DecodedPath path

data Snapshot
    = SnapshotCap
        { snapshotCap :: CHK.Reader
        }
    | Snapshot
        { snapshotContentCap :: CHK.Reader
        , snapshotMetadataCap :: CHK.Reader
        }
    deriving (Show)

type Grid = Map.Map StorageServerID StorageServerAnnouncement

data FolderEntry
    = Directory
        { directoryChildren :: Map.Map T.Text FolderEntry
        }
    | File
        { fileContent :: CHK.Reader
        , fileMetadata :: CHK.Reader
        }
    | Conflict
        { conflictParticipants :: [Participant]
        }
    deriving (Show, Eq)

{- | Represent a reference to the state belonging to a particular participant
 in the Magic-Folder.
-}
data ParticipantCap
    = -- | A writeable participant reference.  Since write capabilities are not
      -- meant to be shared, if we have this one, we consider it to be
      -- "(our)self".
      Self (Directory.DirectoryCapability SDMF.Writer)
    | -- | A read-only participant reference.
      Other (Directory.DirectoryCapability SDMF.Reader)
    deriving (Show, Eq)

newtype Participant = Participant
    { participantPersonalCap :: ParticipantCap
    }
    deriving (Show, Eq)

data MagicFolder = MagicFolder
    { folderCollectiveReadCap :: Directory.DirectoryCapability SDMF.Reader
    , -- | Just the write capability for our personal directory in the
      -- magic-folder, or Nothing if we are a read-only participant (and so have
      -- no such directory).
      ourPersonalWriteCap :: Maybe (Directory.DirectoryCapability SDMF.Writer)
    }
    deriving (Eq, Ord, Show)

{- | Get the unified view of the most recent version of all the files across
 all the participants.
-}
getFolderFiles :: MonadIO m => Grid -> MagicFolder -> m (Either T.Text FolderEntry)
getFolderFiles grid MagicFolder{folderCollectiveReadCap, ourPersonalWriteCap} = do
    collectiveE <- downloadDirectory grid folderCollectiveReadCap announcementToMutableStorageServer
    case collectiveE of
        Left err -> pure $ Left $ T.pack $ show err
        Right collective -> do
            let participants = directoryToParticipants collective ourPersonalWriteCap
            snapshots <- liftIO $ mapConcurrently (getParticipantSnapshots grid) participants
            -- XXX oops throw away a bunch of errors related to interpreting participants
            let snapshots' = rights snapshots
            -- map "mangled entry name" -> Snapshot -- TODO XXX Finish this
            -- XXX oops throw away a bunch of errors related to interpreting snapshots
            let snapshot = Map.mapMaybe rightToMaybe $ head snapshots' :: Map.Map Path Snapshot
                -- XXX oops throw away a bunch of errors related to converting snapshots to entries
                entries = Map.mapMaybe snapshotToEntry snapshot
                -- XXX oops throw away a bunch of errors related to interpreting paths
                parsedEntries = mapMaybe (\(a, b) -> (,b) <$> rightToMaybe a) $ Map.toList entries

            pure . Right . unflatten $ parsedEntries

unflatten :: [(DecodedPath, FolderEntry)] -> FolderEntry
unflatten = foldr (uncurry addToHierarchy) Directory{directoryChildren = mempty}

-- | Merge an entry into the correct position in a directory hierarchical.
addToHierarchy :: DecodedPath -> FolderEntry -> FolderEntry -> FolderEntry
addToHierarchy _ _ File{} = error "addToHierarchy cannot add children to a File"
addToHierarchy _ _ Conflict{} = error "addToHierarchy cannot add children to a Conflict"
addToHierarchy (DecodedPath []) _ _ = error "addToHierarchy needs a location in the hierarchy!"
addToHierarchy (DecodedPath [basename]) entry d = Directory{directoryChildren = newChildren}
  where
    newChildren = Map.insert basename entry (directoryChildren d)
addToHierarchy (DecodedPath (parent : rest)) entry d = Directory{directoryChildren = newChildren}
  where
    newChildren = Map.insert parent updatedSubDir (directoryChildren d)
    updatedSubDir = addToHierarchy remainingPath entry nextSubDir
    remainingPath = DecodedPath rest
    nextSubDir = Map.findWithDefault (Directory mempty) parent (directoryChildren d)

{- | Convert a Snapshot where its content and metadata capabilities are
 already know into a FolderEntry (always a File).

 XXX Not all Snapshots fall into this category and this function just gives
 up for any that don't.
-}
snapshotToEntry :: Snapshot -> Maybe FolderEntry
snapshotToEntry Snapshot{snapshotContentCap, snapshotMetadataCap} = Just $ File snapshotContentCap snapshotMetadataCap
snapshotToEntry _ = Nothing

{- | Parse a general Tahoe-LAFS directory as a "participant" directory in a
 magic-folder and return the magic-folder paths and associated snapshot
 capabilities for its entries.
-}
parseParticipantDirectory ::
    Directory.Directory ->
    [ ( Path
      , Either (ParseErrorBundle T.Text Void) (Directory.DirectoryCapability CHK.Reader)
      )
    ]
parseParticipantDirectory = fmap (bimap (decodePath . Directory.entryName) parse') . entryPairs
  where
    entryPairs = uncurry zip . dupe . filter (not . isMetadata) . Directory.directoryChildren
    dupe a = (a, a)
    parse' = Text.Megaparsec.parse Directory.pReadCHK "extract-snap" . T.decodeLatin1 . Directory.entryReader

    isMetadata :: Directory.Entry -> Bool
    isMetadata Directory.Entry{entryName = "@metadata"} = True
    isMetadata _ = False

-- | Get all the files for just one participant.
getParticipantSnapshots :: forall m. MonadIO m => Grid -> Participant -> m (Either T.Text (Map.Map Path (Either T.Text Snapshot)))
getParticipantSnapshots grid participant = do
    entriesE <-
        downloadDirectory
            grid
            ( case participant of
                Participant (Self writeCap) -> diminishDirectory writeCap
                Participant (Other readcap) -> readcap
            )
            announcementToMutableStorageServer

    case entriesE of
        Left err -> pure $ Left $ T.pack $ show err
        Right d -> do
            snapshots <- mapM download mfDir
            pure $ Right $ Map.fromList snapshots
          where
            mfDir = parseParticipantDirectory d :: [(Path, Either (ParseErrorBundle T.Text Void) (Directory.DirectoryCapability CHK.Reader))]

            download ::
                (Path, Either (ParseErrorBundle T.Text Void) (Directory.DirectoryCapability CHK.Reader)) ->
                m (Path, Either T.Text Snapshot)
            download (name, Left err) = pure (name, Left $ T.pack $ show err)
            download (name, Right dircap) = do
                snapshot <- downloadSnapshot grid dircap
                pure (name, snapshot)

{- | Given the read capability for a directory representing a snapshot,
 retrieve the information and return a local representation.
-}
downloadSnapshot :: MonadIO m => Grid -> Directory.DirectoryCapability CHK.Reader -> m (Either T.Text Snapshot)
downloadSnapshot grid readcap = do
    r <- downloadDirectory grid readcap announcementToImmutableStorageServer
    case r of
        Left err -> pure $ Left $ T.concat ["downloadSnapshot: ", T.pack $ show err]
        Right d -> do
            -- find entries
            case (find' d "content", find' d "metadata") of
                (Just content, Just metadata) ->
                    -- convert to capabilities
                    case (parseCap' content, parseCap' metadata) of
                        (Right (contentReader :: CHK.Reader), Right metadataReader) ->
                            pure $ Right $ Snapshot contentReader metadataReader
                        _ -> pure $ Left $ T.pack $ "Parsing one of the caps failed: " <> show content <> " " <> show metadata
                _ -> pure $ Left $ T.pack $ "Failed to find either (or both!) of content or metadata in " <> show d
  where
    find' d name = find (\x -> Directory.entryName x == name) (Directory.directoryChildren d)
    parseCap' d = Text.Megaparsec.parse CHK.pReader "content-cap" (T.decodeLatin1 $ Directory.entryReader d)

-- | Get all of the participants from a magic-folder collective directory.
directoryToParticipants :: Directory.Directory -> Maybe (Directory.DirectoryCapability SDMF.Writer) -> [Participant]
directoryToParticipants d ourCap = go (Directory.directoryChildren d)
  where
    ourReaderBytes = T.encodeUtf8 . confidentiallyShow . diminishDirectory <$> ourCap
    go [] = []
    go (Directory.Entry{entryName = "@metadata", entryReader = "URI:LIT:pmrhmzlsonuw63rchiqdc7i"} : entries) = go entries
    go (Directory.Entry{entryName = "@metadata"} : _entries) = error "unsupported magic folder version"
    go (Directory.Entry{entryReader} : entries)
        -- fromJust is partial but we only use it here once we prove the value
        -- is Just (by having been able to compute entryReader).
        | Just entryReader == ourReaderBytes = Participant (Self (fromJust ourCap)) : go entries
        | otherwise =
            case Text.Megaparsec.parse Directory.pReadSDMF "participant-collective" (T.decodeLatin1 entryReader) of
                -- XXX TODO Better error handling
                Left err -> error $ "Failed to parse participant in collective: " <> show err
                Right participantCap -> Participant (Other participantCap) : go entries

diminishDirectory :: Directory.DirectoryCapability SDMF.Writer -> Directory.DirectoryCapability SDMF.Reader
diminishDirectory (Directory.DirectoryCapability writeCap) = Directory.DirectoryCapability (SDMF.writerReader writeCap)

rightToMaybe :: Either a b -> Maybe b
rightToMaybe (Left _) = Nothing
rightToMaybe (Right b) = Just b
