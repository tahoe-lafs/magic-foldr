module Spec where

import Data.Bifunctor (Bifunctor (second))
import qualified Data.ByteString as B
import Data.Either (isLeft)
import qualified Data.Map as Map
import qualified Data.Text as T
import Hedgehog (MonadGen, diff, forAll, property, tripping)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import System.IO (hSetEncoding, stderr, stdout, utf8)
import qualified Tahoe.CHK.Capability as CHK
import Tahoe.Capability (ConfidentialShowable (confidentiallyShow))
import qualified Tahoe.Directory as Directory
import Tahoe.MagicFoldr (DecodedPath (..), FolderEntry (..), addToHierarchy, decodePath, encodePath, parseParticipantDirectory, unflatten)
import Test.Tasty (TestTree, defaultMain, testGroup)
import Test.Tasty.HUnit (assertBool, assertEqual, testCase)
import Test.Tasty.Hedgehog (testProperty)
import Text.Megaparsec (parse)

-- | Generate logical Magic-Folder entry paths.
decodedPaths :: MonadGen m => m DecodedPath
decodedPaths = DecodedPath <$> Gen.list depthRange (pathSegments segmentRange)
  where
    depthRange = Range.exponential 1 8
    segmentRange = Range.exponential 1 32

-- | Generate individual Magic-Folder path segments.
pathSegments :: MonadGen m => Range.Range Int -> m T.Text
pathSegments segmentRange = Gen.text segmentRange pathSegmentChars

{- | Generate characters suitable for use in individual Magic-Folder path
 segments.
-}
pathSegmentChars :: MonadGen m => m Char
pathSegmentChars = Gen.filterT (/= '/') Gen.unicode

{- | Generate non-conflicted hierarchical (Directories possibly containing
 Directories) FolderEntry values.
-}
hierarchicalEntries :: MonadGen m => m FolderEntry
hierarchicalEntries = Gen.recursive Gen.choice terminalGens recursiveGens
  where
    terminalGens = [files]
    recursiveGens = [directories]

-- A couple made-up CHK capabilities which the tests can drop in places where
-- such things are needed.  Apart from parsing them, we do not currently
-- attempt any interpretation so it doesn't matter that they aren't real.
contentCapability, metadataCapability :: CHK.Reader
(Right contentCapability) = parse CHK.pReader "content capability" "URI:CHK:content45g7wkfpc57mzanoxmy:7grgc3kuni75kek6ose6kyswyi4mvliqjvcivkx4igltprlp5b6q:1:2:3"
(Right metadataCapability) = parse CHK.pReader "content capability" "URI:CHK:metadata5g7wkfpc57mzanoxmy:7grgc3kuni75kek6ose6kyswyi4mvliqjvcivkx4igltprlp5b6q:1:2:3"

-- | Generate regular-file Magic-Folder entries.
files :: MonadGen m => m FolderEntry
files = pure $ File contentCapability metadataCapability

{- | Generate sub-directory Magic-Folder entries, possibly populated with some
 children.
-}
directories :: MonadGen m => m FolderEntry
directories = Directory <$> Gen.map (Range.exponential 1 32) entries

{- | Generate file or sub-directory Magic-Folder entries.  Eventually, this
 should probably also generate conflicts.
-}
entries :: MonadGen m => m (T.Text, FolderEntry)
entries = (,) <$> pathSegments (Range.exponential 1 8) <*> hierarchicalEntries

tests :: TestTree
tests =
    testGroup
        "MagicFoldr"
        [ testProperty "Paths round-trip through encodePath . decodePath" $
            property $ do
                name <- forAll decodedPaths
                tripping name encodePath decodePath
        , testCase "Strings that are not encoded properly fail to parse" $
            assertBool "mis-encoded strings are parsed to Left" (isLeft $ decodePath "@metadata")
        , testCase "Participant directory can be interpreted" $ do
            participantBytes <- B.readFile "test/data/personal.data"
            let (Right directory) = Directory.parse participantBytes
                snapshots = parseParticipantDirectory directory
                -- Smash caps into strings because they're easy to carry
                -- around as test data.
                comparableSnapshots = second (fmap confidentiallyShow) <$> snapshots
            assertEqual "snapshots do not match expected" expectedSnapshots comparableSnapshots
        , testProperty "An entry can be added to a hierarchy" $
            property $ do
                entry <- forAll files
                directory <- forAll directories

                let (Directory newChildren) = addToHierarchy (DecodedPath ["foo"]) entry directory

                diff (Just entry) (==) (Map.lookup "foo" newChildren)
        , testProperty "Flattened directory hierarchies can be re-nested with addToHierarchy" $
            property $ do
                rootEntry <- forAll $ Gen.filter isDirectory hierarchicalEntries
                tripping rootEntry flattenHierarchy (Just . unflatten)
        ]

isDirectory :: FolderEntry -> Bool
isDirectory Directory{} = True
isDirectory _ = False

flattenHierarchy :: FolderEntry -> [(DecodedPath, FolderEntry)]
-- We don't know the name of Conflict or File here so we can't do anything with them
flattenHierarchy Conflict{} = error "uhhhh, can't flatten conflicts, I guess?"
flattenHierarchy File{} = error "can't flatten files either sorry"
-- Once we're into a Directory, though, everything has a name
flattenHierarchy d = concatMap (go (DecodedPath [])) $ Map.toList (directoryChildren d)
  where
    go :: DecodedPath -> (T.Text, FolderEntry) -> [(DecodedPath, FolderEntry)]
    go (DecodedPath parentPath) (entryName, entry@Directory{}) = (thePath, entry) : concatMap (go thePath) (Map.toList (directoryChildren entry))
      where
        thePath = DecodedPath $ parentPath <> [entryName]
    go (DecodedPath parentPath) (entryName, entry) = [(DecodedPath $ parentPath ++ [entryName], entry)]

-- Hard-coded expected value that agrees with the contents of test/data/personal.data
expectedSnapshots :: [(Either a1 DecodedPath, Either a2 T.Text)]
expectedSnapshots =
    [
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "01 - Ton Steine Scherben - Ich will nicht werden was mein Alter ist.flac"])
        , Right "URI:DIR2-CHK:c26dyh5e3zjghybgddk4sdz6ju:7pyzfljvsikwqiaq3qt7vgenn7uz74jip4jva7xe34jxnz7jm3ta:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "02 - Ton Steine Scherben - Verboten.flac"])
        , Right "URI:DIR2-CHK:ivt2j43ermz3yos2rzhzbdz5ta:6hsfn4ej4a3zha3zgp2qnyos6c6yix5ohvwfb6qhjhsepdce5cta:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "03 - Ton Steine Scherben - Feierabend.flac"])
        , Right "URI:DIR2-CHK:5exwthl6hrz4ob5kbzbm6aalvq:eobojcl6rqok5uv4hlwv6ac73vt4gtgjnowykl7iw22psvsgftwa:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "04 - Ton Steine Scherben - Heut' Nacht.flac"])
        , Right "URI:DIR2-CHK:65qt5y4y5qs57m3l2azfer4fcy:kq5chqzbw3kdygeccaabxbzb36hxuuvspnu7htp67tqz226jir4q:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "05 - Ton Steine Scherben - Raus (aus dem Ghetto).flac"])
        , Right "URI:DIR2-CHK:tfulzoipaffd46z77wd3ehfcuy:kf7h6j7htzd6ghpuw467kdrxfayek5p4uwidq3z3h65a5cscmjra:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "06 - Ton Steine Scherben - Ich will ich sein.flac"])
        , Right "URI:DIR2-CHK:xnklwhejzgzr2ysg7wziwjdcsq:owtmjewfphs753msh57mnpdxdupbsqfg64gtogn5xamzirz2yzrq:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "07 - Ton Steine Scherben - Shit-Hit.flac"])
        , Right "URI:DIR2-CHK:cgfcwcdxofbni3diwj6xx5dtai:ramybwdbb46vlpgoeci5o7hs34247f2x3gcmvcls7hih2cv5ssga:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "08 - Ton Steine Scherben - Jenseits von Eden.flac"])
        , Right "URI:DIR2-CHK:zyz3rstpmzhmbnbf7jsfmflzza:3e3pqcjghurdw3lg3agfqof3rb5jljfvjmsk46mksjk4srvxykiq:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "09 - Ton Steine Scherben - La Reponse.flac"])
        , Right "URI:DIR2-CHK:cbuxbbb5y4yolpcmu3vyqofdmq:7fopkvwglttpdrxpntgeesuttntnfcxkvmh7czeb3zxizshtfw3a:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "10 - Ton Steine Scherben - Keine Macht f\252r Niemand.flac"])
        , Right "URI:DIR2-CHK:7dk2yjfbkmcax4rnrnobziuzla:35mtdq7nwu6ygrdpntpuqybqy42qkqzvtljrwrjjvchhjve4gvha:3:5:422"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "Artwork", "Booklet - front & back.jpg"])
        , Right "URI:DIR2-CHK:e2d6eovlytqqns5vwzjz7mawfy:ww7tbkomujdfo575ea4zvioyadwgxgqzg4c546e5qqb6ulgk7i7q:3:5:420"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "Artwork", "Booklet inside.jpg"])
        , Right "URI:DIR2-CHK:sko66ubys6xpeo45j4srgiunua:plztlfgwa623bvoofgv43ws5nosegx4mm4vzbxvubfazm2vzyova:3:5:420"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "Artwork", "back.jpg"])
        , Right "URI:DIR2-CHK:ipku5myqyorxq6zsn2vixvyxcu:jzemk4c5bprlj63gcsuihnifcqukpxzou6tsqcjq2qgbj55lhtfa:3:5:420"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "Artwork", "disc.jpg"])
        , Right "URI:DIR2-CHK:7i7k7q66ueap5p2csxaosjriaq:2x45ii2c2khgalbrworejfmo6qw3wlct2ov6kqgcl2je3re2rjwa:3:5:420"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "Ton Steine Scherben in Berlin 1984 (1991).txt"])
        , Right "URI:DIR2-CHK:6ptsxkyn724wvrakoqb2wjccxq:injnivrtuqxtlbk3z33lhesoummm2w53ofaax7b45aoy7tmizgaa:3:5:416"
        )
    ,
        ( Right (DecodedPath ["Ton Steine Scherben - in Berlin (1994)", "cover.jpg"])
        , Right "URI:DIR2-CHK:zhqnf3zs2wyuinqtzb656qd6de:2obwzfrdxiyvc74ylq54o7tdz3achxgmnvvpfwupeqz5hrmix5xa:3:5:420"
        )
    ]

main :: IO ()
main = do
    -- Hedgehog writes some non-ASCII and the whole test process will die if
    -- it can't be encoded.  Increase the chances that all of the output can
    -- be encoded by forcing the use of UTF-8 (overriding the LANG-based
    -- choice normally made).
    hSetEncoding stdout utf8
    hSetEncoding stderr utf8
    defaultMain tests
